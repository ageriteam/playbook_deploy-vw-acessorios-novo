#!/bin/bash

DATE=`date +%d/%m/%Y" "%H:%M:%S`

cd /var/www/html
/usr/local/bin/php /var/www/html/artisan october:up 2>&1 | tee -a /var/log/october-output.log
echo "${DATE} - october:up executado." >> /var/log/october.log
exit 0